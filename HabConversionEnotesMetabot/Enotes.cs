﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HabConversionEnotesMetabot
{
    public class Enotes
    {
        /// <summary>
		/// Removes the special characters from FirstName LastName and Business Name
		/// </summary>
		/// <param name="input">FirstName, LastName or Business Name</param>
		/// <returns>Returns a string without any special characters </returns>
        public static string RemoveSpecialCharacters(string input)
        {
            Regex r = new Regex("(?:[^a-z0-9 ]|(?<=['\"])s)", RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Compiled);
            return r.Replace(input, String.Empty);
        }
    }
}
